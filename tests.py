import unittest
from models.Firm import Firm
from models.Product import Product
from models.Employee import Employee


class MyTestCase(unittest.TestCase):

    def test_create_firm(self):
        """Тест на создание объекта Firm"""
        firm1 = Firm('kenguru')
        self.assertEqual('kenguru', firm1.name)

    def test_create_product(self):
        """Тест на создание объекта Product"""
        product = Product('Наушники', 500.60, 10)
        self.assertEqual('Наушники', product.name)

    def test_product_totalcost(self):
        """Тест на расчета стоимости товара"""
        product = Product('Наушники', 500.0, 10)
        self.assertEqual(5000.0, product.total_cost())

    def test_create_employee(self):
        """Тест на создание объекта Employee"""
        employer = Employee('Иванов Иван Иванович', 'менеджер', 5, 30000)
        self.assertEqual('Иванов Иван Иванович', employer.name)

    def test_salary_increase(self):
        """Тест прибавки к зарплате"""
        employer = Employee('Иванов Иван Иванович', 'менеджер', 5, 30000)
        employer.salary_increase()
        self.assertEqual(35000, employer.salary)

    def test_add_product_to_firm(self):
        """Тест на добавление товара к фирме"""
        firm1 = Firm('Adidas')
        krosofki = Product('Кроссовки', 500.60, 10)
        firm1.add_product(krosofki)
        self.assertEqual('Кроссовки', firm1.products[0].name)

    def test_add_employee_to_firm(self):
        """Тест на добавление сотрудника в фирму"""
        firm1 = Firm('Adidas')
        employer = Employee('Иванов Иван Иванович', 'менеджер', 5, 30000)
        firm1.add_employee(employer)
        self.assertEqual('Иванов Иван Иванович', firm1.staff[0].name)


if __name__ == '__main__':
    unittest.main()
