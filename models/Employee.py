class Employee:
    """Класс для сотрудников"""

    def __init__(self, name: str, post: str, experience: int, salary: float):
        self.name = name  # ФИО сотрудника
        self.post = post  # должность
        self.experience = experience  # стаж работы
        self.salary = salary  # зарплата

    def salary_increase(self):
        """Прибавка в зависимости от стажа(Да Надбавка не хуже пенсии)"""
        self.salary = self.salary + 1000*self.experience


