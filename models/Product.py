class Product:
    """Класс товара"""
    def __init__(self, name: str, price: float, storage: int):
        self.name = name  # Наименование
        self.price = price  # Цена за ед
        self.storage = storage  # Количество на складе

    def total_cost(self) -> float:
        """Метод считает стоимость товара"""
        cost = float(self.price) * self.storage
        return cost

