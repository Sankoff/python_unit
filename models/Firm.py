from models.Product import Product


class Firm:
    """Класс для фирм"""
    def __init__(self, name: str):
        self.name = name  # название фирмы
        self.staff = []  # список сотрудников
        self.products = []  # список товаров

    def add_employee(self, employee):
        """Добавить сотрудника в фирму"""
        self.staff.append(employee)

    def add_product(self, product: Product):
        """Добавить товар в фирмы"""
        self.products.append(product)



